import redis
import os

REDIS_HOST = os.getenv('REDIS_HOST')
REDIS_PORT = int(os.getenv('REDIS_PORT', 6379))

redis_client = redis.Redis(host=REDIS_HOST, port=REDIS_PORT)

def set_value(key, value):
	redis_client.set(key, value)

def get_value(key):
	return redis_client.get(key)

if __name__ == "__main__":
	set_value('k1', 'Hello!')
	print(get_value('k1'))