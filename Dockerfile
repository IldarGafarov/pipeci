FROM python:3.7.4-alpine3.10

WORKDIR /usr/app

RUN apk add --no-cache libxslt-dev

COPY ./requirements.txt ./

RUN apk add --no-cache --virtual .build-deps build-base python3-dev \
    && pip install --no-cache-dir --upgrade pip \
    && pip install --no-cache-dir -r requirements.txt \
    && apk --purge del .build-deps

COPY ./ ./

CMD ["python", "main.py"]